<?php
/*
    Fecha: 28 de enero 2023
    Objetivo: Se encarga de procesar los datos obtenidos de la vista, para mandarlos a modelo y una vez obtenido el resultado se los enviara nuevamente a vista para mostrar los resultados de manera ordenada 
*/
session_start();
$_SESSION['directorio_vistas'] = 'controlador';

include("../modelo/MySqlConsultaGrid.php");
//include("../vista/encabezado.php");
include("../control/funcion_tabla.php");

function procesarFormulario() {
    // Obtener los valores del formulario
	$ini_mes = isset($_POST["ini_mes"]) ? $_POST["ini_mes"] : '';
        $ini_anio = isset($_POST["ini_anio"]) ? $_POST["ini_anio"] : '';
        $fin_mes = isset($_POST["fin_mes"]) ? $_POST["fin_mes"] : '';
        $fin_anio = isset($_POST["fin_anio"]) ? $_POST["fin_anio"] : '';

    // Validar que al menos un conjunto de datos sea válido
        //ver lo que contiene cada variable
        /*echo "ini_anio: " . $ini_anio . "<br>";
        echo "ini_mes: " . $ini_mes . "<br>";
        echo "fin_anio: " . $fin_anio . "<br>";
        echo "fin_mes: " . $fin_mes . "<br>";*/
    
        // Llama a la función del modelo para generar el reporte
        $resultados = obtenerResultados($ini_mes, $fin_mes, $ini_anio, $fin_anio);
        
	#Hice pruebas de su funcionamiento a 22/07/2024 (Gonzalo)
	if (!$resultados) {

            header("Location: ../vista/error_datos.php");
            die();
	}
	

	
    if(!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes)){
        // Enviar resultados a la vis
   	include("../vista/encabezado.php");     
        mostrarTablaGridXmes($resultados, $ini_mes, $ini_anio);
	include("../vista/pie.php");

    } elseif (!empty($ini_mes) && !empty($ini_anio) && !empty($fin_mes) && !empty($fin_anio)) {
        // Llama a la función del modelo para generar el reporte
        //$resultados = obtenerResultados($ini_mes, $fin_mes, $ini_anio, $fin_anio);
        // Enviar resultados a la vista
   	include("../vista/encabezado.php");     
	mostrarTablaGridXPeriodo($resultados, $ini_mes, $fin_mes, $ini_anio, $fin_anio);    
	include("../vista/pie.php");
	
	// Verificar resultados y mostrar mensaje de error si es necesario
        if (!$resultados) {
	
	    //header("Location: ../vista/error_datos.php");
            //die();

	
		}
    } else {
        
        // Mensaje de error si no se proporcionan datos válidos 
        //header("Location: ../vista/error_parametros.php");
        //die();
    }
}

// Procesar formulario si se ha enviado
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    procesarFormulario();
}
?>
