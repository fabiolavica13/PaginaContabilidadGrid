
<?php


function mostrarTabla($resultados){

    include("../modelo/MySqlCluster.php");
    if (empty($resultados)) {

        header("Location:  ../vista/error_datos.php");
        die();

        return;
    }

    echo '<center>';
    echo ' <button id="botonPDF" class="boton_pdf" >PDF</button>';
    echo ' <button id="botonExcel" class="boton_csv">EXCEL</button>';
    echo ' <button id="botonCSV" class="boton_csv">CSV</button>';
    echo '</center>';

    echo '<br>'; // Espacio en blanco para que la tabla no esté tan pegada al encabezado
    echo '<b>'; // Negritas
    echo '<figure class="wp-block-table is-style-regular">';

    echo '<div class="container my-5">';
        echo '<div class="row">';
    echo '<table id="myTable" class="display" style="width: 100%">';

    echo '<thead>';
    echo '<tr>';
    echo '<th><strong>#</strong></th>';
    foreach ($resultados[0] as $columna => $valor) {
        echo '<th><strong>' . $columna . '</strong></th>';
    }
    echo '</tr>';

  echo '</thead>';
    // Variable para sumar el total de jobs y horas
    $total_jobs = 0;
    $total_horas = 0;

    echo '<tbody>';
//    echo '<tr bgcolor=#D8AEFF>';
//    echo '</tr>';
    foreach ($resultados as $key => $fila) {

        echo '<tr bgcolor="' . ($key % 2 == 0 ? '#ecf4fb' : '') . '">';
       echo '<th>' . ($key + 1) . '</th>';
        foreach ($fila as $columna => $valor) {
            echo '<th>' . $valor . '</th>';
            if ($columna === 'Njobs') {
                $total_jobs += $valor;
            }
            if ($columna === 'Nhoras') {
                $total_horas += $valor;
            }
        }
        echo '</tr>';
    }


    echo '</tbody>';

    echo '<tfoot>';
         echo '<tr>';
  	 echo '<th><b>TOTAL</b></th>';
                if (count($resultados[0]) == 3) {
                echo '<th></th>';
                echo '<th><b>' . $total_jobs . '</b></th>';
                echo '<th><b>' . $total_horas . '</b></th>';
                
		} elseif (count($resultados[0]) == 4) {
                echo '<th></th>';
                echo '<th></th>';
                echo '<th><b>' . $total_jobs . '</b></th>';
                echo '<th><b>' . $total_horas . '</b></th>';
		
		}

         echo '</tr>';
    echo '</tfoot>';

    echo '</table>';
    echo '</div>';
    echo '</div>';

    echo '</b>';
    echo '<div class="container">';

/*   echo '<figcaption class="wp-element-caption">Tabla: Resultados de la consulta. <br>Última actualización: ' . date('d ' . "/" . ' m ' . "/" . ' Y') . '.</figcaption>';
*/

/*
echo '<table class="table table-success table-striped">';

    echo '<tr>';
    echo '<td><b>TOTAL</b></td>';
    echo '<td></td>'; // Celda vacía para la columna intermedia
    echo '<td></td>';
    echo '<td></td>';
echo '<td></td>';
  echo '<td></td>';
    echo '<td></td>';
    echo '<td></td>';
   echo '<td></td>';
    echo '<td></td>';
echo '<td></td>';
  echo '<td></td>';
    echo '<td></td>';

    echo '<td><b>' . $total_jobs . '</b></td>'; // Total de jobs
    echo '<td></td>';
    echo '<td></td>';
    echo '<td></td>';
    echo '<td></td>';
echo '<td></td>';
    echo '<td></td>';


    echo '<td><b>' . $total_horas . '</b></td>'; // Total de horas

    echo '</tr>';
    echo '</table>';
*/

    echo '</div>';
    echo '</figure>';

    echo '<center>';
   echo  '<div>';
   echo '<a href="../control/control_error.php">';
   echo ' <button class="boton_error">Nueva consulta</button>';
   echo '</a>';



   echo '<a href="../index.php">';

echo ' <button class="boton_error">Menu principal</button>';
echo '</a>';
echo '</div>';
echo '</center>';


echo '<script>';

echo 'var table = new DataTable(\'#myTable\', {';


//echo '$(document).ready(function() {';
//echo '$('#myTable').DataTable({';


//echo ' footerCallback: function (row, data, start, end, display) {';
//echo      ' var api = this.api(), data;';


echo 'select: true,';

echo 'dom: \'Bfrtilp\',';

echo 'buttons:  [';
echo '{';
echo 'extend: \'csvHtml5\',';
echo 'class: \'buttons-csv\',';
echo 'titleAttr: \'Exportar csv\',';
echo 'className: \'btn btn-success\',';
echo 'init: function (api, node, config){';
echo '	$(node).hide();';
echo '}';

echo '},';
echo '{';
echo 'extend: \'excelHtml5\',';
echo 'class: \'buttons-excel\',';
echo 'text: "<i class=\'fa-solid fa-file\'></i>",';
echo 'titleAttr: \'Exportar archivo excel\',';
echo 'className: \'btn btn-success\',';
echo 'init: function (api, node, config){';
echo '	$(node).hide();';
echo '}';

echo '},';
echo '{';
echo 'extend: \'pdfHtml5\',';
echo 'class: \'buttons-pdf\',';
echo 'text: "<i class=\'fa-solid fa-file-pdf\'></i>",';
echo 'titleAttr: \'Exportar pdf\',';
echo 'className: \'btn btn-danger\',';
echo 'init: function (api, node, config){';
echo '	$(node).hide();';

echo '}';
echo '},';
echo '],';


/*
echo '    layout: {';
echo '        topStart: {';
echo '            buttons: [\'csv\', \'excel\', \'pdf\']';
echo '        }';
echo '    },'; // Añadido una coma aquí para separar correctamente las propiedades
*/


echo ' language: {';
echo '       url: \'//cdn.datatables.net/plug-ins/2.0.3/i18n/es-MX.json\',';
echo '},';

echo 'order: [[3,\'desc\']],';
echo 'lengthMenu: [10,15,20,25],';
echo 'columnDefs: [{orderable: false, target: [0,1]},  {searchable: false, target: [0,1]}, { width: \'10%\', target: [1]}, { className: \'myTable\', targets: \'_all\' }]';




 
/*
echo '        var intVal = function (i) {';
echo '            return typeof i === 'string' ?';
echo '                i.replace(/[\$,]/g, '') * 1:';
echo     '            typeof i === 'number' ?';
echo     '            i : 0;';
echo       '};';




echo 'total = api';

echo        '.column(4)';
echo        '.data()';
echo        '.reduce(function (a,b){ 
echo	    '	return intVal(a) + intVal(b);';
echo	    '},0)';

 
 
// Update footer
echo    '$(    api.column(4).footer().html('Total');';
echo    '},';


*/

//echo 'pageLength: 5';




echo '});';


echo '    $(\'#botonExcel\').on(\'click\', function() {';
echo '        table.button(\'.buttons-excel\').trigger();';
echo '    });';

echo '    $(\'#botonPDF\').on(\'click\', function() {';
echo '        table.button(\'.buttons-pdf\').trigger();';
echo '    });';

echo '    $(\'#botonCSV\').on(\'click\', function() {';
echo '        table.button(\'.buttons-csv\').trigger();';
echo '});';

echo '</script>';


}



function mostrarTablaClusterXMes($resultados, $cluster, $ini_mes, $ini_anio){


echo '<center>';
echo '<h2 style="color: black;">';
echo 'Cluster: ' . $cluster ;
echo '</h2>';
echo '<h4 style="color: black;">';
echo 'Mes: ' . $ini_mes ;
echo ' ';
echo 'Año: ' . $ini_anio ;
echo '</h4>';
echo '</center>';






    mostrarTabla($resultados);



}

function mostrarTablaClusterXPeriodo($resultados, $cluster, $ini_mes, $fin_mes, $ini_anio, $fin_anio){

    
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo 'Cluster: ' . $cluster ;
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Periodo del : ' . $ini_mes . ' - ' . $ini_anio . '  al ' . $fin_mes . ' - ' . $fin_anio;
    echo '</h4>';
    echo '</center>';


    mostrarTabla($resultados);
}

function mostrarTablaUsuarioXmes($resultados, $usuario, $ini_mes, $ini_anio){

    
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo 'Usuario: ' . $usuario ;
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Mes: ' . $ini_mes ;
    echo ' ';
    echo 'Año: ' . $ini_anio ;
    echo '</h4>';
    echo '</center>';


    mostrarTabla($resultados);
}



function mostrarTablaUsuarioXPeriodo($resultados, $ini_mes, $fin_mes, $ini_anio, $fin_anio){
    // Mostrar los botones fuera del contenedor de la tabla y encabezado
    echo '<div class="custom-container">';
    echo '<div class="custom-row">';
    echo '<div class="col-md-12 text-center">';
    echo '<div id="buttonContainer" class="dt-buttons"></div>';  // Nuevo contenedor para los botones
    echo '</div>';
    echo '</div>';
    echo '</div>';

    // Mostrar el encabezado con la información del usuario y periodo
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo 'Usuario: ' ;
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Periodo del : ' . $ini_mes . ' - ' . $ini_anio . '  al ' . $fin_mes . ' - ' . $fin_anio;
    echo '</h4>';
    echo '</center>';

    // Mostrar la tabla de resultados
    mostrarTabla($resultados);
}


function mostrarTablaTodosXmes($resultados, $ini_mes, $ini_anio){

    
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Mes: ' . $ini_mes ;
    echo ' ';
    echo 'Año: ' . $ini_anio ;
    echo '</h4>';
    echo '</center>';


    mostrarTabla($resultados);
}



/*
function mostrarTablaUsuarioXPeriodo($resultados, $usuario, $ini_mes, $fin_mes, $ini_anio, $fin_anio){

    
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo 'Usuario: ' . $usuario ;
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Periodo del : ' . $ini_mes . ' - ' . $ini_anio . '  al ' . $fin_mes . ' - ' . $fin_anio;
    echo '</h4>';
    echo '</center>';


    mostrarTabla($resultados);
}
*/


function mostrarTablaTodosXPeriodo($resultados, $ini_mes, $fin_mes, $ini_anio, $fin_anio){

    
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo 'GRID UNAM';
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Periodo del : ' . $ini_mes . ' - ' . $ini_anio . '  al ' . $fin_mes . ' - ' . $fin_anio;
    echo '</h4>';
    echo '</center>';


    mostrarTabla($resultados);
}

function mostrarTablaGridXmes($resultados, $ini_mes, $ini_anio){

    
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo 'Grid UNAM' ;
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Mes: ' . $ini_mes ;
    echo ' ';
    echo 'Año: ' . $ini_anio ;
    echo '</h4>';
    echo '</center>';


    mostrarTabla($resultados);
}

function mostrarTablaGridXPeriodo($resultados, $ini_mes, $fin_mes, $ini_anio, $fin_anio){

    
    echo '<center>';
    echo '<h2 style="color: black;">';
    echo 'Grid UNAM ';
    echo '</h2>';
    echo '<h4 style="color: black;">';
    echo 'Periodo del : ' . $ini_mes . ' - ' . $ini_anio . '  al ' . $fin_mes . ' - ' . $fin_anio;
    echo '</h4>';
    echo '</center>';


    mostrarTabla($resultados);
}

?>



