<?php
/*
    Fecha: 16 de enero 2023
    Objetivo: Se encarga de procesar los datos obtenidos de la vista, para mandarlos a modelo y una vez obtenido el resultado se los enviara nuevamente a vista para mostrar los resultados de manera ordenada 
*/

session_start();
$_SESSION['directorio_vistas'] = 'controlador';


//include("../vista/encabezado.php");
include("../modelo/MySqlConsultaCluster.php");
include("../control/funcion_tabla.php");

function procesarFormulario() {

    // Obtener los valores del formulario
    $cluster = isset($_POST["cluster"]) ? $_POST["cluster"] : '';
    $ini_anio =  isset($_POST["ini_anio"]) ? $_POST["ini_anio"] : '';
    $ini_mes =  isset($_POST["ini_mes"]) ? $_POST["ini_mes"] : '';
    $fin_anio =  isset($_POST["fin_anio"]) ? $_POST["fin_anio"] : '';
    $fin_mes = isset($_POST["fin_mes"]) ? $_POST["fin_mes"] : '';

/*echo "Contenido de \$ini_anio: " . $ini_anio . "<br>";
echo "Contenido de \$fin_mes: " . $fin_mes . "<br>";
echo "Contenido de \$fin_anio: " . $fin_anio . "<br>";
echo "Contenido de \$cluster: " . $cluster . "<br>";
echo "Contenido de \$ini_mes: " . $ini_mes . "<br>"; */    
    // Validar que al menos un conjunto de datos sea válido
    if (($ini_mes !== 'Seleccione un mes' && !empty($ini_anio) && !empty($cluster)) || (!empty($ini_mes) && !empty($ini_anio) && !empty($fin_mes) && !empty($fin_anio) && !empty($cluster))) {

        $resultados = obtenerResultados($cluster, $ini_mes, $fin_mes, $ini_anio, $fin_anio);

	
	//Agregado a 22/07/2024 parece funcionar bien
        if (!$resultados) {

            //echo "Error por que no hay datos---- ";
            header("Location: ../vista/error_datos.php");
           die();
        }
	


        if((!empty($cluster) && !empty($ini_anio) && !empty($ini_mes) && empty($fin_mes))) {
        // Enviar resultados a la vista
        //echo 'Tabla mes';
	include("../vista/encabezado.php");
       mostrarTablaClusterXMes($resultados, $cluster, $ini_mes, $ini_anio);
        include("../vista/pie.php");

    } elseif ((!empty($ini_anio) && !empty($ini_mes) && !empty($fin_anio) && !empty($fin_mes) && !empty($cluster))) {
        //echo 'Tabla periodo';
	include("../vista/encabezado.php");
        mostrarTablaClusterXPeriodo($resultados, $cluster, $ini_mes, $fin_mes, $ini_anio, $fin_anio);
        include("../vista/pie.php");
    }


         // Verificar resultados y mostrar mensaje de error si es necesario
            if (!$resultados) {
            
            //echo "Error por que no hay datos---- ";
            header("Location: ../vista/error_datos.php");
            die();
           
        }

    } else {
    
        // Mensaje de error si no se proporcionan datos válidos 
        header("Location: ../vista/error_parametros.php");
        die();
        
    } 
}

// Procesar formulario si se ha enviado
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    procesarFormulario();
}
?>




