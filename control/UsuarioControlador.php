<?php
session_start();
$_SESSION['directorio_vistas'] = 'controlador';


/*
    Fecha: 16 de enero 2023
    Objetivo: Se encarga de procesar los datos obtenidos de la vista, para mandarlos a modelo y una vez obtenido el resultado se los enviara nuevamente a vista para mostrar los resultados de manera ordenada 
*/
include("../modelo/MySqlConsultaUsuario.php");
include("../control/funcion_tabla.php");

function procesarBDUsuario($usuarios){

    // Obtener los nombres de usuario desde el modelo
    $usuarios = obtenerNombresUsuarios();

    return $usuarios;

}


function procesarFormulario() {
	//include("../vista/encabezado.php");
    
	// Obtener los valores del formulario
    $usuario = isset($_POST["usuario"]) ? $_POST["usuario"] : '';
    $todos = isset($_POST["todos"]) && $_POST["todos"] === "todos";

        $ini_mes = isset($_POST["ini_mes"]) ? $_POST["ini_mes"] : '';
        $ini_anio = isset($_POST["ini_anio"]) ? $_POST["ini_anio"] : '';
        $fin_mes = isset($_POST["fin_mes"]) ? $_POST["fin_mes"] : '';
        $fin_anio = isset($_POST["fin_anio"]) ? $_POST["fin_anio"] : '';
	
/*echo "Contenido de \$ini_mes: " . $ini_mes . "<br>";
echo "Contenido de \$ini_anio: " . $ini_anio . "<br>";
echo "Contenido de \$fin_mes: " . $fin_mes . "<br>";
echo "Contenido de \$fin_anio: " . $fin_anio . "<br>";
echo "Contenido de \$usuario: " . $usuario . "<br>";
echo "Contenido de \$todos: " . $todos . "<br>"; */ 

    // Validar que al menos un conjunto de datos sea válido
    if (!empty($ini_mes) && !empty($ini_anio) && !empty($usuario) || !empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($usuario) || !empty($ini_mes) && !empty($ini_anio) && !empty($todos) || !empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($todos) && empty($usuario)){
	//echo "entro por aca en control";

        //$ini_mes = intval($ini_mes);
        //$fin_mes = intval($fin_mes);
        //$ini_anio = intval($ini_anio);
        //$fin_anio = intval($fin_anio);

        //Verificar si el usuaro no existe y mostrar mensaje de error
        if(!verificarUsuario($usuario) && empty($todos)){
            
            header("Location: ../vista/error_usuario.php");
        die();

        } else {

            // Obtener los resultados
            $resultados = obtenerResultados($usuario, $ini_mes, $fin_mes, $ini_anio, $fin_anio, $todos, $resultadoActividad);

	      //Agregado a 22/07/2024 parece funcionar bien 
        if (!$resultados) {

            //echo "Error por que no hay datos---- ";
            header("Location: ../vista/error_datos.php");
           die();
        }


		//echo "Entro a control";
if(!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes) && !empty($usuario)){
                // Utilizar parámetros en la consulta para consulta de usuario por mes
                // Mostrar la tabla y enviar el pie de página
		//echo "Entro en 1";
               include("../vista/encabezado.php");
		mostrarTablaUsuarioXmes($resultados, $usuario, $ini_mes, $ini_anio);
                include("../vista/pie.php");
            } elseif (!empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($usuario)) {
                // Utilizar parámetros en la consulta para consulta de usuario por periodo
                // Mostrar la tabla y enviar el pie de página
		//echo "Entro en 2";

             include("../vista/encabezado.php");
             mostrarTablaUsuarioXPeriodo($resultados, $ini_mes, $fin_mes, $ini_anio, $fin_anio);
		$mesesSinActividad = obtenerMesesSinActividad($ini_mes, $fin_mes, $fin_anio, $ini_anio);
                include("../vista/vistaUsuarioSinActividad.html");
                include("../vista/pie.php");
            } elseif (!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes) && !empty($todos) || !empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($todos) && empty($usuario)) {
                    
                    if(!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes) && !empty($todos)) {
                        // Utilizar parámetros en la consulta para todos los usuarios por mes
                        // Mostrar la tabla y enviar el pie de página
		//echo "Entro en 3";

		        include("../vista/encabezado.php");
                        mostrarTablaTodosXmes($resultados, $ini_mes, $ini_anio);
                        if (!empty($resultadoActividad)) {
    echo "Los meses que no se registró actividad son: " . $resultadoActividad;
}

			include("../vista/pie.php");
			
                    } elseif (!empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($todos) && empty($usuario)) {
                        // Utilizar parámetros en la consulta para todos los usuarios por mes
		//echo "Entro en 4";
		        include("../vista/encabezado.php");
                        mostrarTablaTodosXPeriodo($resultados, $ini_mes, $fin_mes, $ini_anio, $fin_anio);
			$mesesSinActividad = obtenerMesesSinActividad($ini_mes, $fin_mes, $fin_anio, $ini_anio);
			include("../vista/vistaUsuarioSinActividad.html");
			include("../vista/pie.php");
                    }

            } else {

                // Verificar resultados y mostrar mensaje de error si es necesario
                if (!$resultados) {

                    $_SESSION['error_consulta'] = "consulta";
                    header("Location: ../vista/error_consulta.php");
                    die("Error al obtener resultados de la base de datos");
                } 


            }

        }

        if (!$resultados) {
        
            header("Location: ../vista/error_datos.php");
            die();
            } else {
                //Mensaje de error si no se proporcionan datos válidos 
                header("Location: ../vista/error_parametros.php");
                die();
            }
        }
    }


    // Procesar formulario si se ha enviado
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        procesarFormulario();
    }
  

?>






