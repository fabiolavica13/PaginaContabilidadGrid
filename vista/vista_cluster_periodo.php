<!-- Comentarios a 21 de marzo de 2024, versión 8 de php-->




<?php
/* Se inicializan las variables de sesión y se utilza una para determinar la ruta en la que se encuentra el usuario, esto para mostrar correctamente ciertas imagenes y hacer el redirrecionamiento a index posible. Así mismo usando la variable opción se guarda que se encuentra en vista cluster, esto es importante para el manejo de errores y correcto redireecionamiento en caso de repetir una consulta. */

session_start();
$_SESSION['directorio_vistas'] = 'vista';

         if (isset($_GET['opcion'])) {
             // Verificar el valor de 'opcion' y establecer $_SESSION['opcionSeleccionada'] en consecuencia
            if ($_GET['opcion'] == "cluster") {
                    $_SESSION["opcionSeleccionada"] = 'cluster';
                //    echo $_SESSION["opcionSeleccionada"] ;

                }

        }

/* Se importa este encabezado que se usa en el frontend de todo el proyecto*/

include('encabezado.php');
?>

<div id="form-container">
<form class="formulario_clase" id="formulario" action="../control/ClusterControlador.php" method="POST">
    <h2>
         Reporte de uso Grid UNAM por cluster 
    </h2>
	
<br>
<b> <FONT COLOR="red"> Selecciona un cluster (*) </FONT> </b>

<br>
	<ul id="id_li_formulario">
        <p>
            <li class="li_formulario" id="id_li_formulario">
                <label for="cluster">Cluster:</label>
                <select name="cluster" id="id_cluster" required>
                    <option value="">Seleccione un cluster</option>
                    <option value ="DGTIC">DGTIC</option>
                    <option value ="LAMOD">LAMOD</option>
                    <option value ="IAE">IAE</option>
                    <option value ="ICAYCC">ICAYCC</option>
                </select>
            </li>
        </p>

   <p>

<b> <FONT COLOR="red"> Elige al menos el inicio de periodo para consultar el reporte de esa fecha específca (*)</FONT> </b>


<li class="li_formulario">
    <div class="select-container">
        <label for="ini_periodo">Inicio de periodo:</label>
        <select name="ini_mes" id="id_ini_mes" required>
            <option value="">Seleccione un mes</option>
            <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
            <option value="4">Abril</option>
            <option value="5">Mayo</option>
            <option value="6">Junio</option>
            <option value="7">Julio</option>
            <option value="8">Agosto</option>
            <option value="9">Septiembre</option>
            <option value="10">Octubre</option>
            <option value="11">Noviembre</option>
            <option value="12">Diciembre</option>
        </select>
        </div>

	<div class="select-container">
        <label for="ini_anio"></label>
        <select name="ini_anio" id="ini_anio" required>
            <option value="">Seleccione un año</option>
            <?php
            // Obtener el año actual
            $anioActual = date("Y");
            // Imprimir las opciones para los años desde 2022 hasta el año actual
            for ($ini_anio = 2023; $ini_anio <= $anioActual; $ini_anio++) {
                echo "<option value='$ini_anio'>$ini_anio</option>";
            }
            ?>
        </select>
    </div>
</li>

	</p>

	 <p>


<li class="li_formulario">
    <div class="select-container">
        <label for="fin_periodo">Fin de periodo:</label>
        <select name="fin_mes" id="id_fin_mes" >
            <option value="">Seleccione un mes</option>
            <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
            <option value="4">Abril</option>
            <option value="5">Mayo</option>
            <option value="6">Junio</option>
            <option value="7">Julio</option>
            <option value="8">Agosto</option>
            <option value="9">Septiembre</option>
            <option value="10">Octubre</option>
            <option value="11">Noviembre</option>
            <option value="12">Diciembre</option>
        </select>
        </div>

        <div class="select-container">
        <label for="fin_anio"></label>
        <select name="fin_anio" id="fin_anio">
            <option value="">Seleccione un año</option>
            <?php
            // Obtener el año actual
            $anioActual = date("Y");
            // Imprimir las opciones para los años desde 2022 hasta el año actual
            for ($ini_anio = 2023; $ini_anio <= $anioActual; $ini_anio++) {
                echo "<option value='$ini_anio'>$ini_anio</option>";
            }
            ?>
        </select>
    </div>
</li>


	</p>

	<br>	
	 <p>
            <center>
            <button class="boton_error" type="submit">Enviar</button>
            <button class="boton_error" type="reset">Borrar </button>


	<br>
	<br>
	<FONT COLOR="red"><b>(*) Representa un campo obligatorio </b></FONT>



	    </center>
        </p>



</form>
</div>
<center>
 <a href='../index.php'>
         <button class="boton_error">Menu principal</button>
        </a>
</center>







<?php
						
/*Se importa un pie de página que se utiliza en el frontend de todo el proyecto*/

include('pie.php');

?>



