<hr>

<br>

<center>
<div class="site-info text-center">
                         <p class="icenter"><a style="padding-right:17px" href="https://www.unam.mx" target="_new" rel="noopener"><img src="https://grid.unam.mx/wp-content/uploads/2023/01/l_unam.png" width="78px" height="78px" alt="UNAM" /> </a>  <a style="padding-right:17px" href="https://www.tic.unam.mx/" target="_new" rel="noopener"><img src="https://grid.unam.mx/wp-content/uploads/2023/01/dgtic_l_n.png" alt="DGTIC" width="190" height="78" /></a><a style="padding-right:17px" href="http://www.lamod.unam.mx/" target="_new" rel="noopener"><img src="https://grid.unam.mx/wp-content/uploads/2023/03/l_lamod.png" alt="LAMOD" width="208" height="78" /></a> <a style="padding-right:17px" href="https://www.atmosfera.unam.mx/" target="_new" rel="noopener"><img src="https://grid.unam.mx/wp-content/uploads/2023/01/icacc_l.png" alt="Instituto de Ciencias de la Atmósfera y Cambio Climático" width="260" height="78" /></a></p>
 <p><a style="padding-right:17px" href="https://www.astroscu.unam.mx/IA/index.php?lang=es" target="_new" rel="noopener"><img src="https://grid.unam.mx/wp-content/uploads/2023/02/ia_nl.png" alt="Instituto de Astronomía" width="70" height="100" /></a>
	<a style="padding-right:17px" href="https://www.nucleares.unam.mx/" target="_new" rel="noopener"><img src="https://grid.unam.mx/wp-content/uploads/2023/01/icn_l.png" alt="Instituto de Ciencias Nucleares" width="141" height="78" /></a></p>

   <p style="padding-top:15px">Hecho en México. Universidad Nacional Autónoma de México (UNAM). Todos los derechos reservados 2024. Esta página puede ser reproducida con fines no lucrativos, siempre y cuando se cite la fuente completa y su dirección electrónica, y no se mutile; de otra forma requiere permiso previo por escrito de la institución. <a href="https://www.tic.unam.mx/avisosprivacidad/">| Aviso de privacidad |</a><a href="https://grid.unam.mx/index.php/creditos/"> Créditos | </a></p>
</div>

</center>




<script>
$(document).ready(function() {
    $('#myTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                text: '<i class="fa-solid fa-file-csv"></i>',
                titleAttr: 'Exportar csv',
                className: 'btn btn-success'
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fa-solid fa-file-excel"></i>',
                titleAttr: 'Exportar archivo excel',
                className: 'btn btn-success'
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fa-solid fa-file-pdf"></i>',
                titleAttr: 'Exportar pdf',
                className: 'btn btn-danger'
            }
        ],
        language: {
            url: '//cdn.datatables.net/plug-ins/2.0.3/i18n/es-MX.json'
        },
        order: [[3, 'desc']],
        lengthMenu: [10, 15, 20, 25],
        columnDefs: [
            { orderable: false, targets: [0, 1] },
            { searchable: false, targets: [0, 1] },
            { width: '10%', targets: [1] },
            { className: 'myTable', targets: '_all' }
        ]
    });
});
</script>


</html>
