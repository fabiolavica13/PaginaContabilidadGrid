<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consulta Grid UNAM</title>
    
    <link rel="icon" href="https://grid.unam.mx/wp-content/uploads/2023/01/icono_grid.png" sizes="32x32" />

  <link href="https://unpkg.com/vanilla-datatables@latest/dist/vanilla-dataTables.min.css" rel="stylesheet" type="text/css">
  <script src="https://unpkg.com/vanilla-datatables@latest/dist/vanilla-dataTables.min.js" type="text/javascript"></script>
  <script src='build/pdfmake.min.js'></script>
  <script src='build/vfs_fonts.js'></script>
  <script src='https://code.jquery.com/jquery-3.7.1.js'></script>
  <script src='https://cdn.datatables.net/2.0.3/js/dataTables.js'></script>
  <script src='https://cdn.datatables.net/buttons/3.0.1/js/dataTables.buttons.js'></script>
  <script src='https://cdn.datatables.net/buttons/3.0.1/js/buttons.dataTables.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js'></script>
  <script src='https://cdn.datatables.net/buttons/3.0.1/js/buttons.html5.min.js'></script>
   <script src='https://cdn.datatables.net/buttons/3.0.1/js/buttons.print.min.js'></script>  
   <script src='https://cdn.datatables.net/2.0.8/js/dataTables.js'></script>  



 <link href="https://cdn.datatables.net/v/bs5/jszip-3.10.1/dt-2.0.3/b-3.0.1/b-colvis-3.0.1/b-html5-3.0.1/datatables.min.css" rel="stylesheet">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/v/bs5/jszip-3.10.1/dt-2.0.3/b-3.0.1/b-colvis-3.0.1/b-html5-3.0.1/datatables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

<!--
<link href="./vista/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
-->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />



<!--  El estilo de la tabla proviene de aqu� -->
   <style>

/* Estilo para mostrar los selects en la misma línea */
.select-container {
display: inline-block; /* Mostrar en línea /
margin-right: 40px; /* Espacio entre los selects */
}

        table {
          border-collapse: collapse;
          width: 50%;
        }

table thead {
  color: #fff;
  background-color: #010326;
}

        th, td {
          text-align: left;
          padding: 8px;
        }

        tr:nth-child(even) {
          background-color: #33b0ea;
        }

        table {
            margin: 0 auto;
        }

        .container {
            text-align: center;
        }



    </style>

	
<?php

session_start();

if( $_SESSION['directorio_vistas'] == 'index'){
        //echo 'dirname(__FILE__)';
   echo '<link rel="stylesheet" type="text/css" href="./vista/estilos.css"/>';
   echo '<link href="./vista/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">';


}elseif($_SESSION['directorio_vistas'] == 'controlador'){
    echo '<link rel="stylesheet" type="text/css" href="./../vista/estilos.css"/>';
    echo '<link href="./../vista/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">';

}else{
    echo '<link rel="stylesheet" type="text/css" href="estilos.css"/>';
   echo '<link href="bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">';

}



?>


	  <style>
        .image-container {
            text-align: left; Centra el contenido horizontalmente */
        }

        .shifted-image {
            margin-left: 200px; /* Mueve la imagen hacia la derecha */
            display: block; /* Hace que la imagen sea un bloque para aplicar márgenes automáticos */
		
       }
          </style>

</head>
<body>

<div class="image-container">
	
<?php
/* Revisar este caso de llamado a session_start*/

if( $_SESSION['directorio_vistas'] == 'index'){
	//echo 'dirname(__FILE__)';
   	echo '<img src="./vista/imagenes/Logo_grid_unam_b.png" alt="Index" class="shifted-image">';

}elseif($_SESSION['directorio_vistas'] == 'controlador'){
      echo '<a href="./../index.php">';
	     echo '<img src="./../vista/imagenes/Logo_grid_unam_b.png" alt="Controlador" class="shifted-image">';
      echo '</a>';

}else{
	 echo '<a href="./../index.php">';	
        echo '<img src="./imagenes/Logo_grid_unam_b.png" alt="Otro caso" class="shifted-image">';
	  echo '</a>';


}
	

?>		



</div>


<hr>


