<?php
/* Se inicializan las variables de sesión y se utilza una para determinar la ruta en la que  se encuentra el usuario, esto para mostrar correctamente ciertas imagenes y hacer el redirrecionamiento a index posible. Así mismo usando la variable opción se guarda que se encuentra
en vista cluster, esto es importante para el manejo de errores y correcto redireecionamiento en caso de repetir una consulta. */

session_start();
$_SESSION['directorio_vistas'] = 'vistas';
// Verificar si 'opcion' está configurado en la URL
         if (isset($_GET['opcion'])) {
             // Verificar el valor de 'opcion' y establecer $_SESSION['opcionSeleccionada'] en consecuencia
            if ($_GET['opcion'] == "grid") {
                    $_SESSION["opcionSeleccionada"] = 'grid';
                //    echo $_SESSION["opcionSeleccionada"] ;

                }

	}
/* Se importa este encabezado que se usa en el frontend de todo el proyecto*/	
include("encabezado.php")

?>

<div id="form-container">

<form id="formulario" action="../control/GridControlador.php" method="POST">
  <h2>Reporte de uso de todo Grid UNAM</h2>

<br>
<b> <FONT COLOR="red"> Elige al menos el inicio de periodo para consultar el reporte de esa fecha específca (*)</FONT> </b>
<br>

	<p>

            <li class="li_formulario">
        <div class="select-container">

                <label for="ini_mes">Inicio de periodo:</label>
                <select name="ini_mes" id="ini_mes" required>
                    <option value="">Seleccione un mes</option>
                    <option value="1">Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                </select>
        </div>


        <div class="select-container">

	<label for="ini_anio"></label>
                <select name="ini_anio" id="ini_anio" required>
                    <option value="">Seleccione un año</option>
                    <?php
                    // Obtener el año actual
                    $anioActual = date("Y");

                    // Imprimir las opciones para los años desde 2022 hasta el año actual
                    for ($ini_anio = 2023; $ini_anio <= $anioActual; $ini_anio++) {
                        echo "<option value='$ini_anio'>$ini_anio</option>";
                    }
                    ?>
                </select>

        </div>
            </li>
        </p>
<p>

            <li class="li_formulario">
        <div class="select-container">

                <label for="fin_mes">Fin de periodo:</label>
                <select name="fin_mes" id="fin_mes" >
                    <option value="">Seleccione un mes</option>
                    <option value="1">Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                </select>
        </div>


        <div class="select-container">

	<label for="fin_anio"></label>
                <select name="fin_anio" id="fin_anio">
                    <option selected>Seleccione un año</option>
                    <?php
                    // Obtener el año actual
                    $anioActual = date("Y");
                    
                    // Imprimir las opciones para los años desde 2022 hasta el año actual
                    for ($fin_anio = 2023; $fin_anio <= $anioActual; $fin_anio++) {
                        echo "<option value='$fin_anio'>$fin_anio</option>";
                    }
                    ?>
                </select>


        </div>
            </li>
        </p>
	
	 <br>
         <p>
            <center>
            <button class="boton_error" type="submit">Enviar</button>
            <button class="boton_error" type="reset">Borrar </button>
<br>
<br>
<FONT COLOR="red"><b>(*) Representa un campo obligatorio </b></FONT>


            </center>
        </p>



</form>
</div>
<center>
 <a href='../index.php'>
         <button class="boton_error">Menu principal</button>
        </a>
</center>

<?php
/*Se importa un pie de página que se utiliza en el frontend de todo el proyecto*/
include("pie.php")
?>

