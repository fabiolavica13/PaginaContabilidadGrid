
<!-- index.php -->

<?php
/*inicialización pra utilizar variables de sesión, las cuales permitiran saber de que página se proviene para redirigir al usuario o leer de la ruta correcta según en donde se encuentre el usuario */

session_start();

/* Esta variable tiene 3 casos, en este caso le damos a entender al sistema que estamos en index*/
$_SESSION['directorio_vistas'] = 'index';

?>

<?php
/* Se importa una cabecera que se utiliza en el frontend de todas las páginas que forma el proyecto*/	
    include('./vista/encabezado.php');

?>

<nav>




<!-- Aquí se despliega el menu que se utiliza en index,la redirección guarda una variae que será útil en el manejo de errores -->
	
	<a href="./vista/vista_cluster_periodo.php?opcion=cluster" ><button class="boton_error">Reporte cluster</button></a>
	<a href="./vista/vista_usuario_periodo.php?opcion=usuario" ><button class="boton_error">Reporte usuario</button></a>
	<a href="./vista/vista_proyecto_periodo.php?opcion=proyecto" ><button class="boton_error">Reporte por proyecto</button></a>
	<a href="./vista/vista_grid_periodo.php?opcion=grid" ><button class="boton_error">Reporte Grid</button></a>



</nav>


<br>
<center>
<!-- Uso de un collage de imagenes donde se concentra las computadoras que forman Grid UNAM

	<img src="./vista/imagenes/collage_grid_UNAM.jpg" height="40%" width="40%">
-->

<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="./vista/imagenes/compuMIZTLI2.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="./vista/imagenes/ICN_IA.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="./vista/imagenes/IMG_0263.JPG" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="./vista/imagenes/Lamod-001.jpg" class="d-block w-100" alt="...">
    </div>


  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>


</center>

<?php 
/* Al igual que en el caso de encabezado se importa un pie de página que se utiliza ara todo el frontend del proyecto */
    include('./vista/pie.php');
    
	

?>

<script>




</script>


</html>


