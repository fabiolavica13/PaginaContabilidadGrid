<?php

/*
    Fecha: 28 de enero 2023
    Objetivo: Se encarga de realizar la conulta mediante los datos capturados, los cuales pueden ser año o periodo.
*/
include("conexion.php");

$totaljobs = 0;
$totalhoras = 0;




function procesarConsultaMySql($conta_horas) {
    $resultados = array();
    $usuarios = array();

    while ($row = $conta_horas->fetch_assoc()) {
        // Sumar los valores a las variables globales
        $GLOBALS['totaljobs'] += $row["Njobs"];
        $GLOBALS['totalhoras'] += $row["Nhoras"];

        // Ignorar el login "TOTAL"
        if ($row['login'] === "TOTAL") {
            continue; // Saltar al siguiente registro si el login es "TOTAL"
        }

        $login = $row['login'];
        $cluster = $row['cluster'];
        $Njobs = $row['Njobs'];
        $Nhoras = $row['Nhoras'];

        // Verificar si el usuario ya existe en el array de usuarios y el cluster es el mismo
        if (isset($usuarios[$login]) && $usuarios[$login]['cluster'] === $cluster) {
            // Si el usuario ya existe, sumar las Nhoras y Njobs
            $usuarios[$login]['Njobs'] += $Njobs;
            $usuarios[$login]['Nhoras'] += $Nhoras;
        } else {
            // Si el usuario no existe, agregarlo al array de usuarios
            $usuarios[$login] = array('cluster' => $cluster, 'Njobs' => $Njobs, 'Nhoras' => $Nhoras);
        }
    }

    // Convertir el array asociativo en un array indexado
    foreach ($usuarios as $login => $data) {
        $resultados[] = array(
            'login' => $login,
            'Njobs' => $data['Njobs'],
            'Nhoras' => $data['Nhoras']
        );
    }

    // print($resultados);  // Comentado para evitar imprimir y afectar el flujo
    return $resultados;
}








/*function procesarConsultaMySql($conta_horas) {
    $resultados = array();

    while ($row = $conta_horas->fetch_assoc()) {
        // Sumar los valores a las variables globales
        $GLOBALS['totaljobs'] += $row["Njobs"];
        $GLOBALS['totalhoras'] += $row["Nhoras"];

	// Ignorar el login "TOTAL"
        if ($login === "TOTAL") {
            continue; // Saltar al siguiente registro si el login es "TOTAL"
        }

        $resultados[] = array(
            'login' => $row['login'],
            'cluster' => $row["cluster"],
            'Njobs' => $row["Njobs"],
            'Nhoras' => $row["Nhoras"]
        );

    }
    // print($resultados);  // Comentado para evitar imprimir y afectar el flujo
    return $resultados;
}
*/

function obtenerResultados($ini_mes, $fin_mes, $ini_anio, $fin_anio) {
    

    // Verificar que los parámetros son válidos antes de realizar la consulta
    if(!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes) || !empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio)){

        $con = conectar();
        if (!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes)) {
            // Utilizar parámetros en la consulta
            $conta_horas = $con->query("SELECT * FROM rgrid WHERE rgrid.mes='$ini_mes' and rgrid.anio='$ini_anio' ORDER BY cluster");
            $resultados = procesarConsultaMySql($conta_horas);

        }elseif (!empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio)) {
             $conta_horas = $con->query("SELECT * FROM rgrid WHERE (anio >= '$ini_anio' and mes >= '$ini_mes') and (anio <= '$fin_anio' and mes <= '$fin_mes') order by mes");
$resultados = procesarConsultaMySql($conta_horas);

        }
        // Aquí puedes imprimir o devolver los resultados 
        //print_r($resultados);

        // Cerrar la conexión después de usarla
        $con->close();
        return $resultados; 

    }      
}
