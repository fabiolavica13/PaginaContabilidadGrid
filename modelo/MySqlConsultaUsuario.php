<?php
include("conexion.php");


// Inicializa $totaljobs y $totalhoras como variables globales
$totaljobs = 0;
$totalhoras = 0;

function verificarUsuario($usuario) {
    $con = conectar();
    $ValidarUsuario = $con->query("SELECT * FROM rgrid WHERE rgrid.login='$usuario'");
    $con->close();
    return $ValidarUsuario->num_rows > 0;
}



function procesarConsultaMySql($conta_horas) {
    $resultados = array();
    $usuarios = array(); // Array para almacenar la suma de horas y trabajos por cada usuario y cluster

    while ($row = $conta_horas->fetch_assoc()) {
        $login = $row["login"];
        $cluster = $row["cluster"];
        $Njobs = $row["Njobs"];
        $Nhoras = $row["Nhoras"];

        // Ignorar el login "TOTAL"
        if ($login === "TOTAL") {
            continue; // Saltar al siguiente registro si el login es "TOTAL"
        }

        // Verificar si el usuario ya existe en el array de usuarios y el cluster es el mismo
        if (isset($usuarios[$login][$cluster])) {
            // Si el usuario y el cluster ya existen, sumar las Nhoras y Njobs
            $usuarios[$login][$cluster]['Njobs'] += $Njobs;
            $usuarios[$login][$cluster]['Nhoras'] += $Nhoras;
        } else {
            // Si el usuario o el cluster no existen, agregarlos al array de usuarios
            $usuarios[$login][$cluster] = array('Njobs' => $Njobs, 'Nhoras' => $Nhoras);
        }
    }

    // Convertir el array asociativo en un array indexado
    foreach ($usuarios as $login => $clusters) {
        foreach ($clusters as $cluster => $totalUsuario) {
            $resultados[] = array(
                'login' => $login,
                'cluster' => $cluster,
                'Njobs' => $totalUsuario['Njobs'],
                'Nhoras' => $totalUsuario['Nhoras']
            );
        }
    }

    return $resultados;
}



function convertirNumeroAMes($numero_mes, $anio) {
    $meses = array(
        1 => 'Enero',
        2 => 'Febrero',
        3 => 'Marzo',
        4 => 'Abril',
        5 => 'Mayo',
        6 => 'Junio',
        7 => 'Julio',
        8 => 'Agosto',
        9 => 'Septiembre',
        10 => 'Octubre',
        11 => 'Noviembre',
        12 => 'Diciembre'
    );

    // Verificar si el número de mes existe en el arreglo de meses
    if (isset($meses[$numero_mes])) {
        return $meses[$numero_mes] . " " . $anio;
    }
    return null;
}

// Función para obtener los meses sin actividad
function obtenerMesesSinActividad($ini_mes, $fin_mes, $fin_anio, $ini_anio) {
    // Inicializar un array para almacenar los meses sin actividad
    $mesesSinActividad = array();

    // Conexión a la base de datos
    $con = conectar();

    // Consulta SQL para obtener los meses sin actividad en el período especificado
    $query = "SELECT DISTINCT mes, anio FROM rgrid
              WHERE (anio > '$ini_anio' OR (anio = '$ini_anio' AND mes >= '$ini_mes'))
              AND (anio < '$fin_anio' OR (anio = '$fin_anio' AND mes <= '$fin_mes'))";

    // Ejecutar la consulta SQL
    $result = $con->query($query);

    // Almacenar los meses con actividad registrada en un array
    $mesesConActividad = array();
    while ($row = $result->fetch_assoc()) {
        $mesesConActividad[] = array('mes' => intval($row['mes']), 'anio' => intval($row['anio']));
    }

    // Generar una lista de los meses del período (ini_mes a fin_mes)
    $mesesDelPeriodo = array();
    for ($anio = $ini_anio; $anio <= $fin_anio; $anio++) {
        $mes_ini = ($anio == $ini_anio) ? $ini_mes : 1;
        $mes_fin = ($anio == $fin_anio) ? $fin_mes : 12;
        for ($mes = $mes_ini; $mes <= $mes_fin; $mes++) {
            $mesesDelPeriodo[] = array('mes' => $mes, 'anio' => $anio);
        }
    }

    // Comparar los meses del período con los meses que tienen actividad
    // y almacenar aquellos que no tienen actividad
    foreach ($mesesDelPeriodo as $mesAnio) {
        if (!in_array($mesAnio, $mesesConActividad)) {
            $mesesSinActividad[] = $mesAnio;
        }
    }

    // Cerrar la conexión a la base de datos
    $con->close();

    // Convertir los números de los meses a nombres de meses con años
    $nombres_meses = array();
    foreach ($mesesSinActividad as $mesAnio) {
        $nombre_mes = convertirNumeroAMes($mesAnio['mes'], $mesAnio['anio']);
        if ($nombre_mes !== null) {
            $nombres_meses[] = $nombre_mes;
        }
    }

    // Devolver los nombres de los meses sin actividad
    return $nombres_meses;
}



function obtenerNombresUsuarios() {
    //conexion a la base de datos
    $con = conectar();

    // Consulta para obtener los nombres de usuarios
    $sql = "SELECT login FROM rgrid";

    // Ejecutar la consulta
    $resultado = $con->query($sql);

    // Verificar si se obtuvieron resultados
    if ($resultado->num_rows > 0) {
        // Array para almacenar los nombres de usuarios
        $usuarios = array();

        // Obtener cada fila de resultados y guardar el nombre de usuario en el array
        while ($fila = $resultado->fetch_assoc()) {
            // Verificar si el login ya existe en el arreglo
            if (!in_array($fila['login'], $usuarios)) {
                // Agregar el login al arreglo solo si no existe previamente
                $usuarios[] = $fila['login'];
            }
        }

        // Ordenar los nombres de usuario de forma ascendente
        sort($usuarios);

        // Cerrar la conexión a la base de datos
        $con->close();

        // Devolver el array con los nombres de usuarios ordenados
        return $usuarios;
    } else {
        // Si no se encontraron usuarios, devolver un array vacío
        return array();
    }
}



function obtenerResultados($usuario, $ini_mes, $fin_mes, $ini_anio, $fin_anio, $todos, $resultadoActividad){
    // Verificar que los parámetros son válidos antes de realizar la consulta
	//echo "Si llego por aca";
    // Validar que al menos un conjunto de datos sea válido
    if (!empty($ini_mes) && !empty($ini_anio) && !empty($usuario) || !empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($usuario) || !empty($ini_mes) && !empty($ini_anio) && empty($fin_mes) && !empty($todos) || !empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($todos) && empty($usuario)){	
//echo "cumplio con alguna de las condiciones";        
        $con = conectar();
        if (!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes) && !empty($usuario)) {
            // Utilizar parámetros en la consulta para mes, año y usuario
            $conta_horas = $con->query("SELECT * FROM rgrid WHERE rgrid.mes='$ini_mes' and rgrid.anio='$ini_anio' and rgrid.login='$usuario' ORDER BY login");
	//echo "Ingreso en 1";        
    $resultados = procesarConsultaMySql($conta_horas);
	//return $resultados;
        } elseif (!empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($usuario)) {
            // Utilizar parámetros en la consulta para consulta de usuario por periodo
            $conta_horas = $con->query("SELECT * FROM rgrid WHERE (rgrid.anio >= '$ini_anio' AND rgrid.mes >= '$ini_mes') AND (rgrid.anio <= '$fin_anio' AND rgrid.mes <= '$fin_mes') AND rgrid.login = '$usuario' ORDER BY rgrid.anio, rgrid.mes;");
            $resultados = procesarConsultaMySql($conta_horas);
	//echo "Ingreso en 2";
        } elseif (!empty($ini_mes) && !empty($ini_anio) && empty($fin_mes) && !empty($todos)) {
            // Utilizar parámetros en la consulta para todos los usuarios con mes y año
            $conta_horas = $con->query("SELECT * FROM rgrid WHERE rgrid.mes='$ini_mes' and rgrid.anio='$ini_anio' ORDER BY login;");
            $resultados = procesarConsultaMySql($conta_horas);
	//echo "Ingreso en 3";
        } elseif (!empty($ini_mes) && !empty($fin_mes) && !empty($ini_anio) && !empty($fin_anio) && !empty($todos) && empty($usuario)) {
            // Utilizar parámetros en la consulta para todos los usuarios por periodo
            $conta_horas = $con->query("SELECT * FROM rgrid WHERE (rgrid.anio >= '$ini_anio' AND rgrid.mes >= '$ini_mes') AND (rgrid.anio <= '$fin_anio' AND rgrid.mes <= '$fin_mes') ORDER BY rgrid.anio, rgrid.mes;");
            $resultados = procesarConsultaMySql($conta_horas);

	//echo "Ingreso en 4";
	//print_r($resultados);
	$resultadoActividad = obtenerMesesSinActividad($ini_mes, $fin_mes, $fin_anio, $ini_anio);



        }
	
	// Obtener usuarios sin uso en los períodos especificados
      //print_r($resultados);
        // Cerrar la conexión después de usarla
        $con->close();
        
        return $resultados;
    }
}

?>

